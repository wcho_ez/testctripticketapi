function OrderFormInfo(DistributorOrderID, ThirdPayType, DistributionChannelID, AllianceInfo, ContactInfo, OrderDeliverInfo, PassengerInfoList, OrderInfo) {
  this.DistributorOrderID = DistributorOrderID;
  this.ThirdPayType = ThirdPayType;
  this.DistributionChannelID = DistributionChannelID;
  this.AllianceInfo = AllianceInfo;
  this.ContactInfo = ContactInfo;
  this.OrderDeliverInfo = OrderDeliverInfo;
  this.PassengerInfoList = [PassengerInfoList];
  this.OrderInfo = OrderInfo;
}

function AllianceInfo(AllianceID, SID, UserID, OUID) {
  this.AllianceID = AllianceID;
  this.SID = SID;
  this.UserID = UserID;
  if(OUID){
    this.OUID = OUID;
  }
}

function ContactInfo(ContactName, ContactMobile, Longitude, Latitude, SourceID, ContactTel, ContactFax, ContactEmail, ContactAddress) {
  this.ContactName = ContactName;
  this.ContactMobile = ContactMobile;
  this.Longitude = Longitude;
  this.Latitude = Latitude;
  this.SourceID = SourceID;
  this.ContactTel = ContactTel;
  this.ContactFax = ContactFax;
  this.ContactEmail = ContactEmail;
  this.ContactAddress = ContactAddress;
}

function OrderDeliverInfo(DeliverType, ContactTel, Address, ReceiverName, PostCode, CityName, IsNeedInvoice, Detail, Title, Remark) {
  this.DeliverType = DeliverType;
  this.ContactTel = ContactTel;
  this.Address = Address;
  this.ReceiverName = ReceiverName;
  this.PostCode = PostCode;
  this.CityName = CityName;
  this.IsNeedInvoice = IsNeedInvoice;
  this.Detail = Detail;
  this.Title = Title;
  this.Remark = Remark;
}

function OrderInfo(ActivityID, ChargeType, ServerFrom, ChildNumber, AdultNumber, DepositAmount, Amount, OptionList, OrderSource, ClientIP, Remark, RemarkTitle) {
  this.ActivityID = ActivityID;
  this.ChargeType = ChargeType;
  this.ServerFrom = ServerFrom;
  this.ChildNumber = ChildNumber;
  this.AdultNumber = AdultNumber;
  this.DepositAmount = DepositAmount;
  this.Amount = Amount;
  this.OptionList = OptionList;
  this.OrderSource = OrderSource;
  this.ClientIP = ClientIP;
  this.Remark = Remark;
  this.RemarkTitle = RemarkTitle;
}

function OptionList(OptionID, UseDate, EndDate, Quantity, Price, DepositQuantity, UnitQuantity){
  this.OptionID = OptionID;
  this.UseDate = UseDate;
  this.EndDate = EndDate;
  this.Quantity = Quantity;
  this.Price = Price;
  this.DepositQuantity = DepositQuantity;
  this.UnitQuantity = UnitQuantity;
}

function PassengerInfoList(OptionId, CustomerInfoId, CName, EName, IdCardNo, CardCity, BirthDate, BirthCity, AgeType, IdCardType,
                           Gender, ContactInfo, VisaOrgan, VisaCountry, PassportType, PassportNo, PassportDate, Nationality, IssueDate,
                           InfoId, IdCardTimelimit, NationalityName, HongKongMacauLaissezPasser, Weight, Height, ShoesSize, MyopiaDegree) {
  this.OptionId = OptionId;
  this.CustomerInfoId = CustomerInfoId;
  this.IdCarCNamedType = CName;
  this.EName = EName;
  this.IdCardNo = IdCardNo;
  this.CardCity = CardCity;
  this.BirthDate = BirthDate;
  this.BirthCity = BirthCity;
  this.AgeType = AgeType;
  this.IdCardType = IdCardType;
  this.Gender = Gender;
  this.ContactInfo = ContactInfo;
  this.VisaOrgan = VisaOrgan;
  this.VisaCountry = VisaCountry;
  this.PassportType = PassportType;
  this.PassportNo = PassportNo;
  this.PassportDate = PassportDate;
  this.Nationality = Nationality;
  this.IssueDate = IssueDate;
  this.InfoId = InfoId;
  this.IdCardTimelimit = IdCardTimelimit;
  this.NationalityName = NationalityName;
  this.HongKongMacauLaissezPasser = HongKongMacauLaissezPasser;
  this.Weight = Weight;
  this.Height = Height;
  this.ShoesSize = ShoesSize;
  this.MyopiaDegree = MyopiaDegree;
}
