function OrderInfo(UserInfo, OrderFormInfo, AllianceInfo, ContactInfo, PassengerInfoList) {
  this.UserInfo = UserInfo;
  this.OrderFormInfo = OrderFormInfo;
  this.AllianceInfo = AllianceInfo;
  this.ContactInfo = ContactInfo;
  this.PassengerInfoList = PassengerInfoList;
}

function UserInfo(UID) {
  this.UID = UID;
}

function OrderFormInfo(Distributor_OrderID, DistributionChannelID, ScenicSpotID, PayMode, ThirdPatType, PeopleNumber, Amount, ServerFrom, ResourceInfoList) {
  this.Distributor_OrderID = Distributor_OrderID;
  this.DistributionChannelID = DistributionChannelID;
  this.ScenicSpotID = ScenicSpotID;
  this.PayMode = PayMode;
  this.ThirdPatType = ThirdPatType;
  this.PeopleNumber = PeopleNumber;
  this.Amount = Amount;
  this.ServerFrom = ServerFrom;
  this.ResourceInfoList = ResourceInfoList;
}

function ResourceInfoList(ResourceID, UseDate, Quantity, Price, ItemAddInfos) {
  this.ResourceID = ResourceID;
  this.UseDate = UseDate;
  this.Quantity = Quantity;
  this.Price = Price;
  this.ItemAddInfos = ItemAddInfos;
}

function ItemAddInfos(ResourceOrderExtendInfoID, AddInfoDetails, ID, Content) {
  this.ResourceOrderExtendInfoID = ResourceOrderExtendInfoID;
  this.AddInfoDetails = AddInfoDetails;
  this.ID = ID;
  this.Content = Content;
}

function AllianceInfo(AllianceID, SID, OUID) {
  this.AllianceID = AllianceID;
  this.SID = SID;
  this.OUID = OUID;
}

function ContactInfo(Name, Tel, Fax, Mobile, Email, Address) {
  this.Name = Name;
  this.Tel = Tel;
  this.Fax = Fax;
  this.Mobile = Mobile;
  this.Email = Email;
  this.Address = Address;
}

function PassengerInfoList(CName, IdCardNo, IdCardType, AgeType, BirthDate, BirthCity, CardCity, EName, ContactInfo, Gender, VisaOrgan, PassportDate, VisaCountry, PassportNo, IssueDate, IdCardTimeLimit, PassportType, Nationlity, ResourceID) {
  this.CName = CName;
  this.IdCardNo = IdCardNo;
  this.IdCardType = IdCardType;
  this.AgeType = AgeType;
  this.BirthDate = BirthDate;
  this.BirthCity = BirthCity;
  this.CardCity = CardCity;
  this.EName = EName;
  this.ContactInfo = ContactInfo;
  this.Gender = Gender;
  this.VisaOrgan = VisaOrgan;
  this.PassportDate = PassportDate;
  this.VisaCountry = VisaCountry;
  this.PassportNo = PassportNo;
  this.IssueDate = IssueDate;
  this.IdCardTimeLimit = IdCardTimeLimit;
  this.PassportType = PassportType;
  this.Nationlity = Nationlity;
  this.ResourceID = ResourceID;
}
