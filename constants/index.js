
const ACTIVITY_API_MAP = [
  {
    name: '2.4 玩乐全/增量产品查询',
    desc: '分销商可通过该接口全/增量获取景点 ID',
    icode: {
      test: '83dae0f81f894e8fa69a407834fa6ba4',
      prod: '3d7dd9ab82a0498c9d58c067bea4c515'
    },
    url: '/activity/list',
    data: [
      {name: 'fLUT', value: new Date().toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
      {name: 'Limit', value: '2', type: 'hidden'},
      {name: 'CanSaleExtend', value: '1', type: 'hidden'}
    ]
  },
  {
    name: '2.5 玩乐产品详情',
    desc: '分销商可通过该接口查询产品详细信息',
    icode: {
      test: '73caa9a658484405a9d36400517a85b1',
      prod: '2b9d47251e974084a0bb3ff889a9b62e'
    },
    url: '/activity/product-detail',
    data: [
     {name: 'ProductIDList', value: '4672057', type: 'text'},
     {name: 'DistributionChannel', value: '9', type: 'hidden'},
     {name: 'ResponseDataType', value: '3', type: 'hidden'},
     {name: 'ImageSizeList', value: 'C_500_280_90', type: 'hidden'}
    ]
  },
  {
    name: '2.6 玩乐可选项详情',
    desc: '分销商可通过该接口查询玩乐可选项详细信息',
    icode: {
      test: 'df3c7ae99d434c31b86a0aa347dc86d9',
      prod: 'c785fffa07fc489e811fcb9879188940'
    },
    url: '/activity/option-detail',
    data: [
     {name: 'OptionIDList', value: '4672257', type: 'text'},
     {name: 'RequestOption', value: '0', type: 'hidden'},
     {name: 'DistributionChannelID', value: '9', type: 'hidden'}
    ]
  },
  {
    name: '2.7 玩乐价格日历信息',
    desc: '分销商可通过该接口查询玩乐可选项价格日历信息',
    icode: {
      test: '00e70d9107594a2fb63d54f491da11df',
      prod: '6a6cd72d4deb443ca9b8555a5b4e9abe'
    },
    url: '/activity/option-calendarInfo',
    data: [
     {name: 'OptionIDList', value: '4672257', type: 'text'},
     {name: 'StartDate', value: new Date().toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
     {name: 'EndDate', value: new Date('30 dec 2016 UTC').toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
     {name: 'DistributionChannelID', value: '9', type: 'hidden'}
    ]
  },
  {
    name: '2.8 玩乐落地价格日历信息',
    desc: '分销商可通过该接口落地玩乐可选项价格日历信息',
    icode: {
      test: '6a2652d93265441a99729db70eed8552',
      prod: 'dcf59bb76ab1417682d0f09adc5bba5a'
    },
    url: '/activity/option-price',
    data: [
     {name: 'OptionIDList', value: '4672257', type: 'text'},
     {name: 'StartDate', value: new Date().toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
     {name: 'EndDate', value: new Date('30 dec 2016 UTC').toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
     {name: 'DistributionChannelID', value: '9', type: 'hidden'}
    ]
  },
  {
    name: '2.9 玩乐动态表单',
    desc: '分销商可通过该接口获取玩乐动态表单',
    icode: {
      test: '6ef0b9b4bdc94300a89c796c5974711b',
      prod: '9f63e2343c124d5f87742decba43c905'
    },
    url: '/activity/option-orderInfo',
    data: [
     {name: 'OptionIDList', value: '4672257', type: 'text'}
    ]
  },
  {
    name: '2.10 玩乐创建订单',
    desc: '分销商调用该接口通知携程扣款及创建玩乐订单。',
    icode: {
      test: '0c746f35652d4fee94f5af8361fa85e8',
      prod: 'aa16b152b9fe432eba72682e4ec8a414'
    },
    url: '/activity/create-order',
    data: [
      {name: 'dummy', value: '1', type: 'hidden'}
    ]
  },
  {
    name: '2.11 用户UID生成',
    desc: '通过该接口查询已生成用户UID',
    icode: {
      test: 'e26bc561ac644aac9a806cfe883a7f33',
      prod: '27f2055a974841da8f771a4a0ddbf4d3'
    },
    url: '/activity/generate-uid',
    data: [
      {name: 'TelphoneNumber', value: '13800138000', type: 'hidden'},
      {name: 'AllianceID', type: 'hidden'},
      {name: 'SID', type: 'hidden'}
    ]
  },
  {
    name: '2.12 玩乐订单状态变化增量查询',
    desc: '通过该接口获得查询时间内订单状态变化的订单',
    icode: {
      test: '388363f0513a42bfa31046c6acbdce18',
      prod: 'e1c7914f691543a79069d71f44808f52'
    },
    url: '/activity/order-status',
    data: [
     {name: 'BeginOrderTime', value: new Date().toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
     {name: 'EndOrderTime', value: new Date('30 dec 2016 UTC').toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
     {name: 'PageSize', value: '10', type: 'hidden'},
     {name: 'PageIndex', value: '1', type: 'hidden'},
     {name: 'DistributionChannelID', value: '9', type: 'hidden'},
     {name: 'AllianceID', value: '262754', type: 'hidden'},
     {name: 'SID', value: '711535', type: 'hidden'}
    ]
  },
  {
    name: '2.13 玩乐订单基本信息查询',
    desc: '通过该接口查询已生成的玩乐订单基本信息',
    icode: {
      test: '5ac4bf49e6b146469876859455687cda',
      prod: '23c2586792ff4b9780606287e596f8cf'
    },
    url: '/activity/order-detail',
    data: [
     {name: 'OrderIds', value: '2340123139', type: 'text'},
     {name: 'DistributionChannelID', value: '9', type: 'hidden'},
     {name: 'AllianceID', value: '262754', type: 'hidden'},
     {name: 'SID', value: '711535', type: 'hidden'}
    ]
  },
  {
    name: '2.14 玩乐退订',
    desc: '分销商提交退单申请。',
    icode: {
      test: 'fb5ed1b1e1a0400e8e3081f5dd7a98d3',
      prod: 'f08f73c5468c43b0963c284f763bba8f'
    },
    url: '/activity/order-refundReq',
    data: [
     {name: 'DistributionChannelID', value: '9', type: 'hidden'},
     {name: 'OrderId', value: '2340137580', type: 'text'},
     {name: 'Reason', value: 'Reason', type: 'hidden'},
     {name: 'Remark', value: 'Remark', type: 'hidden'},
     {name: 'OrderItemID', type: 'hidden'},
     {name: 'Quantity', type: 'hidden'}
    ]
  },
  {
    name: '2.15 玩乐可退检查',
    desc: '分销商通过该接口查询玩乐订单是否可退',
    icode: {
      test: '532edd0629584aeba8e46ca277053b18',
      prod: 'ad9e622b36cb40199ea8834a3d8d2543'
    },
    url: '/activity/order-refundChk',
    data: [
     {name: 'OrderId', value: '2340137580', type: 'text'},
     {name: 'OptionID', type: 'hidden'},
     {name: 'Quantity', type: 'hidden'},
     ]
  },
  {
    name: '2.16 玩乐短信内容',
    desc: '分销商通过该接口查询玩乐短信内容',
    icode: {
      test: 'e6a730cb591b46c494b5194e4e96e634',
      prod: '488e3f39a2474a03a1bda4f95acc1697'
    },
    url: '/activity/order-confirmInfo',
    data: [
     {name: 'OrderId', value: '2340137580', type: 'text'}
     ]
  },
  {
    name: '2.17 玩乐重发短信',
    desc: '分销商通过该接口请求供应商重发短信',
    icode: {
      test: 'e1c731f110e2455c84164513e9ddb442',
      prod: '47163ecb9f904b638011472550f9cdeb'
    },
    url: '/activity/order-reSendsms',
    data: [
     {name: 'OrderId', value: '2340137580', type: 'text'},
     {name: 'DistributionChannelId', value: '9', type: 'hidden'}
     ]
  },
  {
    name: '2.18 玩乐退订规则',
    desc: '分销商通过该接口获取退订规则',
    icode: {
      test: '02467d0177444f50964993d71ff2e39f',
      prod: 'b0e68babf8634bfaa7ef24425c3651a7'
    },
    url: '/activity/refund-policy',
    data: [
     {name: 'ActivityID', value: '2322003', type: 'text'},
     {name: 'AllianceID', type: 'hidden'},
     {name: 'OptionIDList', value: '2322006', type: 'text'}
    ]
  },
  {
    name: '2.19 玩乐退订流水详情',
    desc: '分销商通过该接口获取退订流水详情',
    icode: {
      test: '9cdb006623e14380b182c172123cc6b3',
      prod: '07f15ccdd58746d8a5e85f4a8be94834'
    },
    url: '/activity/order-refund',
    data: [
     {name: 'OrderID', value: '', type: 'text'},
     {name: 'AllianceID', value: '262754', type: 'hidden'},
     {name: 'SID', value: '711535', type: 'hidden'}
    ]
  }
];

// cancelOrder 13b1101283604e669e47053cd8f7ec3a
// checkCancelOrder 35b829a77ebd4553a785f474f8e7db3f
// getJntOrderDetail a6e5ae2899f8442b8797558751607b8c
// getOrderStatus 9b22f04c55e749b684d1a86e41b44672
// jntBooking 856a3642f7844396bb676b0ff2e7b2ab
// jntGetProduct 16de42d894b243398ca834e32892919c
// jntPrebooking d60b962b457e49cb919c51e648a90e64
// jntSearchProducts 3b7244dfad6c4fc3890e536224a28324
// jntSearchRecommendPrices f32be0dfd4f646aca6dd59f6fcfb4386
// 4.1.1 查询国际接送机城市信息 oCHGetCityInfos ba82280dbc8d4d538476a0854dd1ff01
// 4.1.2 查询国际接送机机场信息 oCHGetLocationInfos c5bdc78afe674a4ab72078917303e242
// oCHGetVehicleGroupInfos 270c97fba8b440bd960ebe47c51a6902
// oCHGetVendorInfos 86f76ea04aa843f5b0c9ef50916b554b
// searchOrders f75d77280e53478998db74ee1f646ada

const CAR_API_MAP = [
  {
    name: '2.4 玩乐全/增量产品查询',
    desc: '分销商可通过该接口全/增量获取景点 ID',
    icode: {
      test: '83dae0f81f894e8fa69a407834fa6ba4',
      prod: '3d7dd9ab82a0498c9d58c067bea4c515'
    },
    url: '/activity/list',
    data: [
      {name: 'fLUT', value: new Date().toISOString().substring(0, 10) + 'T00:00:00', type: 'hidden'},
      {name: 'Limit', value: '2', type: 'hidden'},
      {name: 'CanSaleExtend', value: '1', type: 'hidden'}
    ]
  }
];
module.exports = global.ACTIVITY_API_MAP = ACTIVITY_API_MAP;
module.exports = global.CAR_API_MAP = CAR_API_MAP;
