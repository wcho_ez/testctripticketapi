var express = require('express');
var bodyParser = require('body-parser');
var request = require("request");
var async = require('async');
const RETURNORDER_API_MAP = require('../constants');


const authEndpoint = "https://openserviceauth.ctrip.com/OpenServiceAuth/";
const ctripEndpoint = "https://sopenservice.ctrip.com/OpenService/";
const authService = "authorize.ashx";
const proxyService = "ServiceProxy.ashx";


/** start 携程玩乐业务API *************************************/
const sUUID = "683b-b072-944d-a39c-c85d-cdb6-636d31a33619";
const iAID = global.ACT_iAID = 262754;
const iSID = global.ACT_iSID = 711535;
const sKEY = global.ACT_sKEY = "6e866bf5aa3649f4bd1f0a3f17f00b67";

const authWrapper = function(url){
  return url + "&Token=" + global.Act_Access_Token;
}

const returnOrderAPI = express.Router();

// returnOrderAPI.get('/', function(req, res){
//   var data = {
//     Access_Token: global.Access_Token,
//     Refresh_Token: global.Refresh_Token
//   };
//   res.render('activity-api',data);
// });



// ajax(root)
returnOrderAPI.post('/auth', function(req, res){
    // console.log(req.body);
    async.series([
    /*
     * First external endpoint
     */
    function(callback) {
      request({
        uri: authEndpoint + authService + "?AID=" + iAID + "&SID=" + iSID + "&KEY=" + sKEY,
          method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
      }, function(err, response, body) {
        if(err) { console.log(err); callback(true); return; }
        obj = JSON.parse(body);
        callback(false, obj);
      });
    }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }

      console.log("Access_Token: " + results[0].Access_Token);
      console.log("Refresh_Token: " + results[0].Refresh_Token);
      global.Act_Access_Token = results[0].Access_Token;
      global.Act_Refresh_Token = results[0].Refresh_Token;
      res.send(results[0]);
    }
    );
});

returnOrderAPI.post('/refresh', function(req, res){
    // console.log(req.body);
    async.series([
    /*
     * First external endpoint
     */
    function(callback) {
      request({
        uri: authEndpoint + "/refresh.ashx?AID=" + iAID + "&SID=" + iSID + "&refresh_token=" + req.query.token,
        method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
      }, function(err, response, body) {
        if(err) { console.log(err); callback(true); return; }
        obj = JSON.parse(body);
        callback(false, obj);
      });
    }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }

      console.log("Access_Token: " + results[0].Access_Token);
      console.log("Refresh_Token: " + results[0].Refresh_Token);
      global.Act_Access_Token = results[0].Access_Token;
      global.Act_Refresh_Token = results[0].Refresh_Token;
      res.send(results[0]);
    }
    );
});

// 2.4 ticket list 玩乐全/增量产品查询
returnOrderAPI.get('/list', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("fLUT: " + req.query.fLUT);
    console.log("Limit: " + req.query.Limit);
    console.log("CanSaleExtend: " + req.query.CanSaleExtend);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";

    sURL = authWrapper(sURL);
    console.log("sURL: " + sURL);

    var requestData = {"LastUpdateTime":req.query.fLUT,
                       "Limit":req.query.Limit,
                       "CanSaleExtend":req.query.CanSaleExtend};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    });
    // END: Response after dealing with API
});

module.exports = returnOrderAPI;


