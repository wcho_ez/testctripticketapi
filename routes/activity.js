var express = require('express');
var bodyParser = require('body-parser');
var request = require("request");
var async = require('async');
const ACTIVITY_API_MAP = require('../constants');


const authEndpoint = "https://openserviceauth.ctrip.com/OpenServiceAuth/";
const ctripEndpoint = "https://sopenservice.ctrip.com/OpenService/";
const authService = "authorize.ashx";
const proxyService = "ServiceProxy.ashx";


/** start 携程玩乐业务API *************************************/
const sUUID = "683b-b072-944d-a39c-c85d-cdb6-636d31a33619";
const iAID = global.ACT_iAID = 262754;
const iSID = global.ACT_iSID = 711535;
const sKEY = global.ACT_sKEY = "6e866bf5aa3649f4bd1f0a3f17f00b67";

const authWrapper = function(url){
  return url + "&Token=" + global.Act_Access_Token;
}

const activityAPI = express.Router();

// activityAPI.get('/', function(req, res){
//   var data = {
//     Access_Token: global.Access_Token,
//     Refresh_Token: global.Refresh_Token
//   };
//   res.render('activity-api',data);
// });



// ajax(root)
activityAPI.post('/auth', function(req, res){
    // console.log(req.body);
    async.series([
    /*
     * First external endpoint
     */
    function(callback) {
      request({
        uri: authEndpoint + authService + "?AID=" + iAID + "&SID=" + iSID + "&KEY=" + sKEY,
          method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
      }, function(err, response, body) {
        if(err) { console.log(err); callback(true); return; }
        obj = JSON.parse(body);
        callback(false, obj);
      });
    }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }

      console.log("Access_Token: " + results[0].Access_Token);
      console.log("Refresh_Token: " + results[0].Refresh_Token);
      global.Act_Access_Token = results[0].Access_Token;
      global.Act_Refresh_Token = results[0].Refresh_Token;
      res.send(results[0]);
    }
    );
});

activityAPI.post('/refresh', function(req, res){
    // console.log(req.body);
    async.series([
    /*
     * First external endpoint
     */
    function(callback) {
      request({
        uri: authEndpoint + "/refresh.ashx?AID=" + iAID + "&SID=" + iSID + "&refresh_token=" + req.query.token,
        method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
      }, function(err, response, body) {
        if(err) { console.log(err); callback(true); return; }
        obj = JSON.parse(body);
        callback(false, obj);
      });
    }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }

      console.log("Access_Token: " + results[0].Access_Token);
      console.log("Refresh_Token: " + results[0].Refresh_Token);
      global.Act_Access_Token = results[0].Access_Token;
      global.Act_Refresh_Token = results[0].Refresh_Token;
      res.send(results[0]);
    }
    );
});

// 2.4 ticket list 玩乐全/增量产品查询
activityAPI.get('/list', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("fLUT: " + req.query.fLUT);
    console.log("Limit: " + req.query.Limit);
    console.log("CanSaleExtend: " + req.query.CanSaleExtend);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";

    sURL = authWrapper(sURL);
    console.log("sURL: " + sURL);

    var requestData = {"LastUpdateTime":req.query.fLUT,
                       "Limit":req.query.Limit,
                       "CanSaleExtend":req.query.CanSaleExtend};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    });
    // END: Response after dealing with API
});

// 2.5 detail 玩乐产品详情
activityAPI.get('/product-detail', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("ProductIDList: " + req.query.ProductIDList);
    console.log("DistributionChannel: " + req.query.DistributionChannel);
    console.log("ResponseDataType: " + req.query.ResponseDataType);
    console.log("ImageSizeList: " + req.query.ImageSizeList);

    // ScenicSpotIDList: 1~20 items
    // DistributionChannelID: 9 (fixed)
    // ResponseDataType: 0 [default], 1,2
    // ImageSizeKey: C_500_280_90

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var IDList = req.query.ProductIDList;
    if(IDList){
        var ssList = IDList.split(/[\n\r,]+/);
        var ssListLen = ssList.length;
        var trimedList = [];
        var cc = 0;
        for (var i = 0; i < ssListLen; i++) {
            console.log(ssList[i]);
            if(ssList[i]){
              var thisOne = ssList[i].toString().replace(/\D/g,'');
              if(thisOne!==''){
                trimedList[cc] = thisOne;
                cc++;
              }
            }
        }
        IDList = trimedList.join(",");
    }
    else{
        IDList = "4672057,10000255";
    }

    var requestData = {"ProductIDList": IDList.split(","),
                       "DistributionChannel": req.query.DistributionChannel,
                       "ResponseDataType": req.query.ResponseDataType,
                       "ImageSizeList": req.query.ImageSizeList};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.6 option-detail 玩乐可选项详情
activityAPI.get('/option-detail', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OptionIDList: " + req.query.OptionIDList);
    console.log("RequestOption: " + req.query.RequestOption);
    console.log("DistributionChannelID: " + req.query.DistributionChannelID);

    // OptionIDList: 1~20 items
    // RequestOption: 0[default], 1,2,4,8,16
    // DistributionChannelID: 9 (fixed)

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var IDList = req.query.OptionIDList;
    if(IDList){
        var ssList = IDList.split(/[\n\r,]+/);
        var ssListLen = ssList.length;
        var trimedList = [];
        var cc = 0;
        for (var i = 0; i < ssListLen; i++) {
            console.log(ssList[i]);
            if(ssList[i]){
              var thisOne = ssList[i].toString().replace(/\D/g,'');
              if(thisOne!==''){
                trimedList[cc] = thisOne;
                cc++;
              }
            }
        }
        IDList = trimedList.join(",");
    }
    else{
        IDList = "4672257,2322006";
    }

    var requestData = {"OptionIDList": IDList.split(","),
                       "RequestOption": req.query.RequestOption,
                       "AllianceID": iAID,
                       "SID": iSID,
                       "DistributionChannelID": req.query.DistributionChannelID};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.7 calendarInfo 玩乐价格日历信息
activityAPI.get('/option-calendarInfo', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OptionIDList: " + req.query.OptionIDList);
    console.log("StartDate: " + req.query.StartDate);
    console.log("EndDate: " + req.query.EndDate);
    console.log("DistributionChannelID: " + req.query.DistributionChannelID);

    // OptionIDList: 1~20 items
    // StartDate: 
    // EndDate: 
    // DistributionChannelID: 9 (fixed)

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var IDList = req.query.OptionIDList;
    if(IDList){
        var ssList = IDList.split(/[\n\r,]+/);
        var ssListLen = ssList.length;
        var trimedList = [];
        var cc = 0;
        for (var i = 0; i < ssListLen; i++) {
            console.log(ssList[i]);
            if(ssList[i]){
              var thisOne = ssList[i].toString().replace(/\D/g,'');
              if(thisOne!==''){
                trimedList[cc] = thisOne;
                cc++;
              }
            }
        }
        IDList = trimedList.join(",");
    }
    else{
        IDList = "4672257,2322006";
    }

    var requestData = {"OptionIDList": IDList.split(","),
                       "StartDate": req.query.StartDate,
                       "EndDate": req.query.EndDate,
                       "AllianceID": iAID,
                       "SID": iSID,
                       "DistributionChannelID": req.query.DistributionChannelID};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.8 calendarInfo 玩乐落地价格日历信息
activityAPI.get('/option-price', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OptionIDList: " + req.query.OptionIDList);
    console.log("StartDate: " + req.query.StartDate);
    console.log("EndDate: " + req.query.EndDate);
    console.log("DistributionChannelID: " + req.query.DistributionChannelID);

    // OptionIDList: 1~20 items
    // StartDate: 
    // EndDate: 
    // DistributionChannelID: 9 (fixed)

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var IDList = req.query.OptionIDList;
    if(IDList){
        var ssList = IDList.split(/[\n\r,]+/);
        var ssListLen = ssList.length;
        var trimedList = [];
        var cc = 0;
        for (var i = 0; i < ssListLen; i++) {
            console.log(ssList[i]);
            if(ssList[i]){
              var thisOne = ssList[i].toString().replace(/\D/g,'');
              if(thisOne!==''){
                trimedList[cc] = thisOne;
                cc++;
              }
            }
        }
        IDList = trimedList.join(",");
    }
    else{
        IDList = "4672257,2322006";
    }

    var requestData = {"OptionIDList": IDList.split(","),
                       "StartDate": req.query.StartDate,
                       "EndDate": req.query.EndDate,
                       "AllianceID": iAID,
                       "SID": iSID,
                       "DistributionChannelID": req.query.DistributionChannelID};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.9 order DynamicInfo 玩乐动态表单
activityAPI.get('/option-orderInfo', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OptionIDList: " + req.query.OptionIDList);

    // OptionIDList: 1~20 items

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var IDList = req.query.OptionIDList;
    if(IDList){
        var ssList = IDList.split(/[\n\r,]+/);
        var ssListLen = ssList.length;
        var trimedList = [];
        var cc = 0;
        for (var i = 0; i < ssListLen; i++) {
            console.log(ssList[i]);
            if(ssList[i]){
              var thisOne = ssList[i].toString().replace(/\D/g,'');
              if(thisOne!==''){
                trimedList[cc] = thisOne;
                cc++;
              }
            }
        }
        IDList = trimedList.join(",");
    }
    else{
        IDList = "4672257,2322006";
    }

    var requestData = {"OptionIDList": IDList.split(","),
                       "AllianceID": iAID,
                       "SID": iSID};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 10. 玩乐创建订单
activityAPI.get('/create-order', function(req, res){
  console.log("fENV: " + req.query.fENV);
  console.log("iCode: " + req.query.iCode);

  var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                         "&UUID=" + sUUID +
                         "&mode=1&format=json";
    sURL = authWrapper(sURL);
  console.log("sURL: " + sURL);
  var orderData;
  if(req.query.order){
    orderData = JSON.parse(req.query.order);
  }else{
    var fs = require('fs');
    orderData = JSON.parse(fs.readFileSync('./views/activity/order.json', 'utf8'));
    orderData['PassengerInfoList'] = [orderData['PassengerInfoList']];
  }

  orderData['DistributorOrderID'] = 'ORD000' + Math.floor(Math.random() * 875);
  // const x1 = orderData['OrderInfo']['OptionList'];
  // var x2 = {
  //     "OptionID": "2322008",
  //     "UseDate": "2016-12-20T00:00:00",
  //     "EndDate": "2016-12-21T00:00:00",
  //     "Quantity": "1",
  //     "Price": "120",
  //     "DepositQuantity": "0",
  //     "DepositPayMode": "0",
  //     "UnitQuantity": "1"
  // };
  // orderData['OrderInfo']['OptionList'] = [x1,x2];
  // orderData['ChildNumber'] = '1';
  // var p1 = orderData['PassengerInfoList'];
  // p1['OptionId'] = '2322008';
  // orderData['PassengerInfoList'] = [p1, orderData['PassengerInfoList']];
  
  // console.log("requestData: " + JSON.stringify(orderData));

  // START: Response after dealing with API
  async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: orderData
        }, function(err, response, body) {
          // console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: orderData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./activity/create-order', renderData);
    }
  );
  // END: Response after dealing with API
});

// 2.11 generate-uid 用户UID生成
activityAPI.get('/generate-uid', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    console.log("sURL: " + sURL);

    var AllianceID = req.query.AllianceID;
    if (!AllianceID) {
      AllianceID = iAID;
    }

    var uSID = req.query.SID;
    if (!uSID) {
      uSID = iSID;
    }

    var TelphoneNumber = req.query.TelphoneNumber;
    if (!TelphoneNumber) {
      TelphoneNumber = '13800138000';
    }

    var requestData = {"TelphoneNumber": TelphoneNumber,
                       "UidKey": sUUID,
                       "AllianceID": AllianceID,
                       "SID": uSID};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          // console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.12 order status 玩乐订单状态变化增量查询
activityAPI.get('/order-status', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("BeginOrderTime: " + req.query.BeginOrderTime);
    console.log("EndOrderTime: " + req.query.EndOrderTime);
    console.log("PageSize: " + req.query.PageSize);
    console.log("PageIndex: " + req.query.PageIndex);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var thisMonth = new Date().getMonth() + 1 == 12 ? 0 : new Date().getMonth() + 1;
    var nextMonth = new Date();
    nextMonth.setMonth(thisMonth + 1);
    var begin = req.query.BeginOrderTime == "" ? new Date().toISOString().substring(0, 10) : req.query.BeginOrderTime;
    var end = req.query.EndOrderTime == "" ? nextMonth.toISOString().substring(0, 10) : req.query.EndOrderTime;
    var pages = req.query.PageSize == "" || req.query.PageSize == 0 ? 1 : req.query.PageSize;
    var pageIn = req.query.PageIndex == "" || req.query.PageIndex == 0 ? 1 : req.query.PageIndex;
    var AllianceID = req.query.AllianceID;
    var uSID = req.query.SID;
    var requestData;
    if(AllianceID && uSID){
      requestData = {"BeginOrderTime": begin,
                    "EndOrderTime":end,
                    "PageSize":pages,
                    "PageIndex":pageIn,
                    "DistributionChannelID": req.query.DistributionChannelID,
                    "AllianceID": AllianceID,
                    "SID": uSID};
    }else{
      requestData = {"BeginOrderTime": begin,
                "EndOrderTime":end,
                "PageSize":pages,
                "PageIndex":pageIn,
                "AllianceID": "",
                "SID": "",
                "DistributionChannelID": req.query.DistributionChannelID};
    }

    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.13 order detail 玩乐订单基本信息查询
activityAPI.get('/order-detail', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OrderIds: " + req.query.OrderIds);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var IDList = req.query.OrderIds;
    if(IDList){
        var ssList = IDList.split(/[\n\r,]+/);
        var ssListLen = ssList.length;
        var trimedList = [];
        var cc = 0;
        for (var i = 0; i < ssListLen; i++) {
            console.log(ssList[i]);
            if(ssList[i]){
              var thisOne = ssList[i].toString().replace(/\D/g,'');
              if(thisOne!==''){
                trimedList[cc] = thisOne;
                cc++;
              }
            }
        }
        IDList = trimedList.join(",");
    }
    else{
        IDList = "2340122850,2340122854";
    }
    var AllianceID = req.query.AllianceID;
    var uSID = req.query.SID;
    var requestData;
    if(AllianceID && uSID){
      requestData = {"OrderIds": IDList.split(","),
                       "AllianceID": AllianceID,
                       "SID": uSID,
                       "DistributionChannelID": req.query.DistributionChannelID};
    }else{
      requestData = {"OrderIds": IDList.split(","),
                       "AllianceID": "",
                       "SID": "",
                       "DistributionChannelID": req.query.DistributionChannelID};
    }
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.14 order refundReq 玩乐退订
activityAPI.get('/order-refundReq', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OrderId: " + req.query.OrderId);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var cancelOrderItem;
    if(req.query.OrderItemID && req.query.Quantity){
      cancelOrderItem = {"OrderItemID": req.query.OrderItemID,
                           "Quantity": req.query.Quantity};
    }else{
      cancelOrderItem = {"OrderItemID": req.query.OrderItemID ? req.query.OrderItemID:"17907921",
                         "Quantity": req.query.Quantity ? req.query.Quantity:"1"};
    }
    var requestData = {"OrderId": req.query.OrderId,
                       "Reason": req.query.Reason,
                       "Remark": req.query.Remark,
                       "CancelOrderItems": cancelOrderItem,
                       "AllianceID": iAID,
                       "SID": iSID,
                       "DistributionChannelID": req.query.DistributionChannelID};
    var inputData = {"OrderId": req.query.OrderId,
                       "Reason": req.query.Reason,
                       "Remark": req.query.Remark,
                       "OrderItemID": cancelOrderItem.OrderItemID,
                       "Quantity": cancelOrderItem.Quantity,
                       "DistributionChannelID": req.query.DistributionChannelID};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: inputData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.15 order refundChk 玩乐可退检查
activityAPI.get('/order-refundChk', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OrderId: " + req.query.OrderId);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var orderItems;
    if(req.query.OptionID && req.query.Quantity){
      orderItems = {"OptionID": req.query.OptionID,
                    "Quantity": req.query.Quantity};
    }else{
      orderItems = {"OptionID": req.query.OptionID ? req.query.OptionID:"17907921",
                    "Quantity": req.query.Quantity ? req.query.Quantity:"1"};
    }
    var requestData = {"OrderId": req.query.OrderId,
                       "Reason": req.query.Reason,
                       "Remark": req.query.Remark,
                       "AllianceID": iAID,
                       "SID": iSID,
                       "OrderItems": orderItems};
    var inputData = {"OrderId": req.query.OrderId,
                       "Reason": req.query.Reason,
                       "Remark": req.query.Remark,
                       "OptionID": orderItems.OptionID,
                       "Quantity": orderItems.Quantity};
    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: inputData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.16 order confirmInfo 玩乐短信内容
activityAPI.get('/order-confirmInfo', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OrderId: " + req.query.OrderId);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    
    var requestData = {"OrderId": req.query.OrderId,
                       "AllianceID": iAID,
                       "SID": iSID,};

    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.17 order reSendsms 玩乐重发短信
activityAPI.get('/order-reSendsms', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OrderId: " + req.query.OrderId);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    
    var requestData = {"OrderId": req.query.OrderId,
                       "AllianceID": iAID,
                       "SID": iSID,
                       "DistributionChannelId": req.query.DistributionChannelID};

    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.18 refund policy 玩乐退订规则
activityAPI.get('/refund-policy', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("ActivityID: " + req.query.ActivityID);
    console.log("OptionIDList: " + req.query.OptionIDList);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);
    var IDList = req.query.OptionIDList;
    if(IDList){
        var ssList = IDList.split(/[\n\r,]+/);
        var ssListLen = ssList.length;
        var trimedList = [];
        var cc = 0;
        for (var i = 0; i < ssListLen; i++) {
            console.log(ssList[i]);
            if(ssList[i]){
              var thisOne = ssList[i].toString().replace(/\D/g,'');
              if(thisOne!==''){
                trimedList[cc] = thisOne;
                cc++;
              }
            }
        }
        IDList = trimedList.join(",");
    }
    else{
        IDList = "2322006";
    }
    var requestData = {"OptionIDList": IDList.split(","),
                       "ActivityID": req.query.ActivityID,
                       "AllianceID": iAID,
                       "SID": iSID};

    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
      // /*
      //  * Second => if any thing then
      //  */
      // function(callback) {
      //   // nothing for now
      // },
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
    // END: Response after dealing with API
});

// 2.19 order refund 玩乐退订流水详情
activityAPI.get('/order-refund', function(req, res){
    console.log("fENV: " + req.query.fENV);
    console.log("iCode: " + req.query.iCode);
    console.log("OrderID: " + req.query.OrderID);

    var sURL = ctripEndpoint + proxyService + "?AID=" + iAID + "&SID=" + iSID + "&ICODE=" + req.query.iCode +
                           "&UUID=" + sUUID +
                           "&mode=1&format=json";
    sURL = authWrapper(sURL);

    var AllianceID = req.query.AllianceID;
    var uSID = req.query.SID;

    var requestData = {"OrderID": req.query.OrderID,
                       "AllianceID": AllianceID,
                       "SID": uSID};

    console.log("requestData: " + JSON.stringify(requestData));

    // START: Response after dealing with API
    async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: "POST",
          json: requestData
        }, function(err, response, body) {
          //console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if(err) { console.log(err); callback(true); return; }
          // console.log("==========================================================================================");
          // console.log(JSON.stringify(body));
          // console.log("==========================================================================================");
          var data = {
            ENV: req.query.fENV,
            ICODE: req.query.iCode,
            RDATA: requestData,
            JSONSTRING: JSON.stringify(body)
          };
          callback(false, data);
        });
      },
    ],
    function(err, results) {
      if(err) { console.log(err); res.send(500,"Server Error"); return; }
      const renderData = results[0];
      const mapIdx = req.query.mapIdx;
      renderData['API_METADATA_IDX'] = mapIdx;
      renderData['API_METADATA'] = ACTIVITY_API_MAP[mapIdx];
      res.render('./common/resultLayout', renderData);
    }
    );
});

module.exports = activityAPI;


