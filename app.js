/*jshint esversion: 6 */
/*
  |--------------------------------------------------------------------------
  | 門票 aid:262753, sid:711534, token:3436cc8bcace4c73b6ab0e4233f9577d
  | 活動 aid:262754, sid:711535, token:6e866bf5aa3649f4bd1f0a3f17f00b67
  |--------------------------------------------------------------------------
  */

global.iAID = 262753;
global.iSID = 711534;
const sKEY = '3436cc8bcace4c73b6ab0e4233f9577d';

const sUUID = '683b-b072-944d-a39c-c85d-cdb6-636d31a33619';
const authEndpoint = 'https://openserviceauth.ctrip.com/OpenServiceAuth/';
const ctripEndpoint = 'https://sopenservice.ctrip.com/OpenService/';
const authService = 'authorize.ashx';
const proxyService = 'ServiceProxy.ashx';

const ctripCarTestEndpoint = 'https://sopenservice.ctrip.com/OpenService/';

global.car_test.iAID = 715620;
global.car_test.iSID = 1256423;
global.car_test.sKEY = 'e95751cd80f44d4aa27f405690ea6f4c';

global.car_prod.iAID = 715619;
global.car_prod.iSID = 1256422;
global.car_prod.sKEY = 'c776bbd7c6a240839aae0083116db104';


global.PKG_TITLE = '[携程]门票&玩乐业务&国际专车接送机&订单详情';

global.FUNC_NM_2_4 = '2.4 門票全/增量景點查詢';
global.FUNC_DESC_2_4 = '分銷商可通過該接口全/增量獲取景點 ID';
global.ICODE_2_4_TEST = '041f592e13f24c359e41da8acfc3178c';
global.ICODE_2_4_PROD = '3e756f999d0a419eac8ed257e9a4cf5b';

global.FUNC_NM_2_5 = '2.5 門票景點詳情';
global.FUNC_DESC_2_5 = '分銷商可通過該接口查詢含門票的景點詳細信息';
global.ICODE_2_5_TEST = '2ff54073468446bfa25e77d3051a2b59';
global.ICODE_2_5_PROD = '07310f189af1477aadcf8ca45f8171a4';

global.FUNC_NM_2_6 = '2.6 門票資源詳情';
global.FUNC_DESC_2_6 = '分銷商可通過該接口查詢含門票的資源詳細信息';
global.ICODE_2_6_TEST = '0a8987905b814b179c415210dc0a82e6';
global.ICODE_2_6_PROD = '9941a51c685a4fa289d3861c5dfbfad3';

global.FUNC_NM_2_8 = '2.8 門票動態表單';
global.FUNC_DESC_2_8 = '分銷商可通過該接口獲取門票動態表單';
global.ICODE_2_8_TEST = '0db502255df140e8a2e8a66c4101a915';
global.ICODE_2_8_PROD = '5e77d1765fe7488085ae82037216d31e';

global.FUNC_NM_2_9 = '2.9 門票價格日歷';
global.FUNC_DESC_2_9 = '分銷商可通過該接口獲取門票價格日歷與庫存信息';
global.ICODE_2_9_TEST = 'd4afc4d925db48bbb0a4de9a3fdd8165';
global.ICODE_2_9_PROD = '8a813070d06f4482be39ea1f349624c5';

global.FUNC_NM_2_10 = '2.10 門票可訂性檢查';
global.FUNC_DESC_2_10 = '分銷商可通過該接口查詢指定門票是否被預訂';
global.ICODE_2_10_TEST = '2adb8f6326c1411eb9458e4fab4c5930';
global.ICODE_2_10_PROD = 'eedc35f1ca1b4751b477496ac00b6f21';

global.FUNC_NM_2_11 = '2.11 門票創建訂單';
global.FUNC_DESC_2_11 = '分銷商調用該接口通知攜程扣款及創建門票訂單';
global.ICODE_2_11_TEST = '05c11efe4f07450ab3c55787f0ca9963';
global.ICODE_2_11_PROD = 'e342195619ba4ca98b642dd5b42dc8fb';

global.FUNC_NM_2_12 = '2.12 用戶UID生成';
global.FUNC_DESC_2_12 = '通過該接口查詢已生成用戶UID';
global.ICODE_2_12_TEST = 'e26bc561ac644aac9a806cfe883a7f33';
global.ICODE_2_12_PROD = '27f2055a974841da8f771a4a0ddbf4d3';

global.FUNC_NM_2_13 = '2.13 門票訂單基本信息查詢';
global.FUNC_DESC_2_13 = '通過該接口查詢已生成的門票訂單基本信息';
global.ICODE_2_13_TEST = '39c75ec2b040426681b3595bfad1f786';
global.ICODE_2_13_PROD = '503e93cef33a4c3caa3cb46621a7fa32';

global.FUNC_NM_2_14 = '2.14 門票訂單狀態變化';
global.FUNC_DESC_2_14 = '分銷商可通過該接口獲取訂單狀態變化的ID';
global.ICODE_2_14_TEST = '32de0d7f6aae46499fef359feac02199';
global.ICODE_2_14_PROD = '0c3edd6a6f934689a844fe911e21d226';

global.FUNC_NM_2_15 = '2.15 門票可退檢查';
global.FUNC_DESC_2_15 = '銷商通過該接口查詢門票訂單是否可退';
global.ICODE_2_15_TEST = '0354f8601b0144ebaf6da82bffbc8b25';
global.ICODE_2_15_PROD = '9ace498c96b8430bbaa7fd45a4ac4c8d';

global.FUNC_NM_2_16 = '2.16 門票退單';
global.FUNC_DESC_2_16 = '分銷商提交退單申請';
global.ICODE_2_16_TEST = '2a8c23e9a336412297c746cb299abfc4';
global.ICODE_2_16_PROD = 'bcb9a8bed0bb446f8db39c3ea5ef817d';

global.FUNC_NM_2_17 = '2.17 門票短信內容';
global.FUNC_DESC_2_17 = '銷商通過該接口查詢門票短信內容';
global.ICODE_2_17_TEST = '22a9d188bbfe4f258525364d65759c1b';
global.ICODE_2_17_PROD = '44e141473ce94b96b7ce4136e619fda2';

global.FUNC_NM_2_18 = '2.18 門票退訂流水詳情';
global.FUNC_DESC_2_18 = '分銷商通過該接口查詢門票短信內容';
global.ICODE_2_18_TEST = '2ebd29c0cc824f4889a3fdb0fb04dc05';
global.ICODE_2_18_PROD = '1a194eb7ad534b2ebc877e2a90aa0ef0';


global.RETORD_iAID = 262754;
global.RETORD_iSID = 711535;
global.RETORD_sKEY = "6e866bf5aa3649f4bd1f0a3f17f00b67";

global.RETORD_FUNC_NM_2_4 = '2.4 订单详情查询';
global.RETORD_FUNC_DESC_2_4 = '分销商可通过该接口获取订单详情';
global.RETORD_ICODE_2_4_TEST = '634da6ccc5d2411d99896f1f6ff30e24';
global.RETORD_ICODE_2_4_PROD = 'f7340096d54e4987b7b01e0d6708247c';


var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var async = require('async');

require('request-debug')(request);

var fs = require('fs');

var app = express();

// Prevent: 'Hostname/IP doesn't match certificate's altnames'
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

// root
app.get('/', function(req, res) {
  var data = {
    Access_Token: global.Access_Token,
    Refresh_Token: global.Refresh_Token,
    Act_Access_Token: global.Act_Access_Token,
    Act_Refresh_Token: global.Act_Refresh_Token
  };
  res.render('main', data);
});

const activityAPI = require('./routes/activity.js');

//const returnOrderAPI = require('./routes/returnOrder.js');
// Apply this router on (/activity)
app.use('/activity', activityAPI);

app.get('/activity-query', function(req, res) {

  var data = {
    Access_Token: global.Access_Token,
    Refresh_Token: global.Refresh_Token
  };
  res.render('activity-query', data);
});

// ajax(root)
app.post('/ticket/auth', function(req, res) {
  // console.log(req.body);
  async.series([
      /*
       * First external endpoint
       */
      function(callback) {
        request({
          uri: authEndpoint + authService + '?AID=' + iAID + '&SID=' + iSID + '&KEY=' + sKEY,
          method: 'GET',
          timeout: 10000,
          followRedirect: true,
          maxRedirects: 10
        }, function(err, response, body) {
          if (err) {
            console.log(err);
            callback(true);
            return;
          }
          obj = JSON.parse(body);
          callback(false, obj);
        });
      }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if (err) {
        console.log(err);
        res.send(500, 'Server Error');
        return;
      }

      console.log('Access_Token: ' + results[0].Access_Token);
      console.log('Refresh_Token: ' + results[0].Refresh_Token);
      global.Access_Token = results[0].Access_Token;
      global.Refresh_Token = results[0].Refresh_Token;
      res.send(results[0]);
    }
  );
});

app.post('/ticket/refresh', function(req, res) {
  // console.log(req.body);
  async.series([
      /*
       * First external endpoint
       */
      function(callback) {
        request({
          uri: authEndpoint + '/refresh.ashx?AID=' + iAID + '&SID=' + iSID + '&KEY=' + sKEY + '&refresh_token=' + req.query.token,
          method: 'GET',
          timeout: 10000,
          followRedirect: true,
          maxRedirects: 10
        }, function(err, response, body) {
          if (err) {
            console.log(err);
            callback(true);
            return;
          }
          obj = JSON.parse(body);
          callback(false, obj);
        });
      }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if (err) {
        console.log(err);
        res.send(500, 'Server Error');
        return;
      }

      console.log('Access_Token: ' + results[0].Access_Token);
      console.log('Refresh_Token: ' + results[0].Refresh_Token);
      global.Access_Token = results[0].Access_Token;
      global.Refresh_Token = results[0].Refresh_Token;
      res.send(results[0]);
    }
  );
});

// 2.4 ticket list
app.get('/ticket/list', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var requestData = {
    'LastUpdateTime': req.query.fLUT,
    'Limit': req.query.Limit,
    'AllianceID': iAID,
    'SID': iSID,
    'CanSaleExtend': req.query.CanSaleExtend
  };

  ezPost(req, res, sURL, requestData, './ticket/list');

});

// 2.5 scenic spot detail
app.get('/ticket/scenicspot-detail', function(req, res) {

  var sURL = ctripEndpoint + proxyService + '?AID=' + iAID + '&SID=' + iSID + '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var IDList = splitIdList(req.query.ScenicSpotIDList);

  var requestData = {
    'ScenicSpotIDList': IDList.split(','),
    'DistributionChannelID': req.query.DistributionChannelID,
    'ResponseDataType': req.query.ResponseDataType,
    'ImageSizeKey': req.query.ImageSizeKey,
    'AllianceID': iAID,
    'SID': iSID
  };

  ezPost(req, res, sURL, requestData, './ticket/scenicspot-detail');

});

// 2.6 ticket resource detail
app.get('/ticket/resource-detail', function(req, res) {

  // 注意:文件裡的參數名稱為 ResouceIDList, 少一個 'r'.

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var IDList = splitIdList(req.query.ResouceIDList);

  var requestData = {
    'ResouceIDList': IDList.split(','),
    'DistributionChannelID': req.query.DistributionChannelID,
    'AllianceID': iAID,
    'SID': iSID,
  };

  ezPost(req, res, sURL, requestData, './ticket/resource-detail');

});

// 2.8 门票动态表单 Resource Dynamic
app.get('/ticket/resource-dynamic', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var IDList = splitIdList(req.query.ResourceIDList);

  var requestData = {
    'ResourceIDList': IDList.split(','),
    'AllianceID': iAID,
    'SID': iSID
  };

  ezPost(req, res, sURL, requestData, './ticket/resource-dynamic');

});

// 2.9 门票价格日历 Resource Price Calendar
app.get('/ticket/resource-price-calendar', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var IDList = splitIdList(req.query.ResourceIDList);

  var StartDate = req.query.StartDate;

  var EndDate = req.query.EndDate;

  var requestData = {
    'ResourceIDList': IDList.split(','),
    'StartDate': StartDate,
    'EndDate': EndDate,
    'AllianceID': iAID,
    'SID': iSID
  };

  ezPost(req, res, sURL, requestData, './ticket/resource-price-calendar');

});

// 2.10 门票可订性检查
app.get('/ticket/resource-order-check', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var requestData;
  if (req.query.ResourceID) {
    requestData = {
      'ResourceInfoList': [{
        'ResourceID': req.query.ResourceID,
        'Quantity': req.query.Quantity,
        'UseDate': req.query.UseDate,
        'Price': req.query.Price,
        'CtripPrice': req.query.CtripPrice
      }],
      'UID': req.query.UID,
      'ContactInfo': {
        'Name': req.query.Name,
        'Mobile': req.query.Mobile,
        'Email': req.query.Email,
      },
      'PassenerInfoList': [{
        'CName': req.query.CName,
        'EName': req.query.EName,
        'ContactInfo': req.query.ContactInfo,
        'IdCardType': req.query.IdCardType,
        'IdCardNo': req.query.IdCardNo,
      }],
      'Amount': req.query.Amount,
      'PayMode': req.query.PayMode,
      'DistributionChannelID': '9',
      'AllianceID': iAID,
      'SID': iSID,
    };
  } else {
    requestData = {
      'ResourceInfoList': [{
        'ResourceID': 227172,
        'Quantity': 1,
        'UseDate': '/Date(1480521600000-0000)/',
        'Price': 50,
        'CtripPrice': 50
      }],
      'UID': '5be652a1-efca-4159-a82f-8122884d6f08',
      'ContactInfo': {
        'Name': ' Ian',
        'Mobile': '13333333333'
      },
      'PassenerInfoList': [{
        'CName': ' 王贺',
        'EName': ' Andy',
        'ContactInfo': '15215478953',
        'IdCardType': 2,
        'IdCardNo': '110000198506144557'
      }],
      'Amount': 50,
      'PayMode': 'P',
      'DistributionChannelID': 9,
      'AllianceID': 262753,
      'SID': 711534
    };
  }

  ezPost(req, res, sURL, requestData, './ticket/resource-order-check');

});


// 2.11 门票创建订单
app.get('/ticket/order-form', function(req, res) {
  var data = {
    ENV: req.query.fENV,
    ICODE: req.query.iCode,
    RDATA: '',
    JSONSTRING: ''
  };
  res.render('./ticket/create-order', data);
});

app.post('/ticket/create-order', function(req, res) {
  // console.log('fENV: ' + req.query.fENV);
  // console.log('iCode: ' + req.query.iCode);

  var sURL = ctripEndpoint + proxyService + '?AID=' + iAID + '&SID=' + iSID + '&ICODE=' + req.body.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';
  console.log('sURL: ' + sURL);

  if (req.body.order) {
    console.log('form data');
    var orderData = req.body.order;

    // var fs = require('fs');
    // orderData = JSON.parse(fs.readFileSync('./views/ticket/order.json', 'utf8'));

    // START: Response after dealing with API
    async.series([
        /*
         * First => deal with API
         */
        function(callback) {
          request({
            uri: sURL,
            method: 'POST',
            json: orderData
          }, function(err, response, body) {
            if (err) {
              console.log(err);
              callback(true);
              return;
            }
            callback(false, new PageData(req.query.fENV, req.query.iCode, orderData, body));
          });
        }
      ],
      /*
       * Collate results
       */
      function(err, results) {
        if (err) {
          console.log(err);
          res.send(500, 'Server Error');
          return;
        }
        res.json(results[0]);
      }
    );
    // END: Response after dealing with API
  }

});


// 2.12 ticket resource detail
app.get('/ticket/generate-uid', function(req, res) {
  console.log('fENV: ' + req.query.fENV);
  console.log('iCode: ' + req.query.iCode);

  var AllianceID = req.query.AllianceID;
  if (!AllianceID) {
    AllianceID = iAID;
  }

  var SID = req.query.SID;
  if (!SID) {
    SID = iSID;
  }

  var UidKey = req.query.UidKey;
  if (!UidKey) {
    UidKey = sUUID;
  }

  var TelphoneNumber = req.query.TelphoneNumber;
  if (!TelphoneNumber) {
    TelphoneNumber = '13800138000';
  }

  var requestData = {
    'TelphoneNumber': TelphoneNumber,
    'UidKey': UidKey,
    'AllianceID': AllianceID,
    'SID': SID
  };

  var sURL = ctripEndpoint + proxyService + '?AID=' + AllianceID + '&SID=' + SID + '&ICODE=' + req.query.iCode +
    '&UUID=' + UidKey +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  ezPost(req, res, sURL, requestData, './ticket/generate-uid');

});


// 2.13 门票订单基本信息查询
app.get('/ticket/order-info', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var OrderId = req.query.OrderId;

  var requestData = {
    'OrderId': OrderId,
    'DistributionChannelID': req.query.DistributionChannelID,
    'AllianceID': iAID,
    'SID': iSID
  };

  ezPost(req, res, sURL, requestData, './ticket/order-info');

});

// 2.14 门票订单状态变化
app.get('/ticket/order-status', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var BeginOrderTime = req.query.BeginOrderTime;

  var EndOrderTime = req.query.EndOrderTime;

  var PageSize = req.query.PageSize;

  var PageIndex = req.query.PageIndex;

  var requestData = {
    'AllianceID': iAID,
    'SID': iSID,
    'BeginOrderTime': BeginOrderTime,
    'EndOrderTime': EndOrderTime,
    'PageSize': PageSize,
    'PageIndex': PageIndex
  };

  ezPost(req, res, sURL, requestData, './ticket/order-status');

});

// 2.15 门票可退检查
app.get('/ticket/cancel-status', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var OrderId = req.query.OrderId;

  var requestData = {
    'OrderId': OrderId,
    'AllianceID': iAID,
    'SID': iSID
  };

  ezPost(req, res, sURL, requestData, './ticket/cancel-status');

});

// 2.16 门票退单
app.get('/ticket/cancel-order', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var OrderId = req.query.OrderId;

  var requestData = {
    'DistributionChannelID': req.query.DistributionChannelID,
    'OrderId': OrderId,
    'Reason': req.query.Reason,
    'Remark': req.query.Remark,
    'AllianceID': iAID,
    'SID': iSID,
    'OrderItemIDs': { // 文件誤植為 'OrderItems'.
      'OrderItemID': req.query.OrderItemID,
      'Quantity': req.query.Quantity
    },
    'OrderVerifys': {
      'VerifyKey': req.query.VerifyKey,
      'VerifyValue': req.query.VerifyValue
    },
    'CancelType': req.query.CancelType
  };

  ezPost(req, res, sURL, requestData, './ticket/cancel-order');

});

// 2.17 门票短信内容
app.get('/ticket/voucher-order-item-info', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var OrderId = req.query.OrderId;

  var requestData = {
    'OrderId': OrderId,
    'AllianceID': iAID,
    'SID': iSID
  };

  ezPost(req, res, sURL, requestData, './ticket/voucher-order-item-info');

});

// 2.18 门票退订流水详情
app.get('/ticket/order-refund-list', function(req, res) {

  var sURL = ctripEndpoint + proxyService +
    '?AID=' + iAID +
    '&SID=' + iSID +
    '&ICODE=' + req.query.iCode +
    '&UUID=' + sUUID +
    '&Token=' + global.Access_Token +
    '&mode=1&format=json';

  var OrderId = req.query.OrderId;

  var requestData = {
    'OrderId': OrderId,
    'AllianceID': iAID,
    'SID': iSID
  };

  ezPost(req, res, sURL, requestData, './ticket/order-refund-list');

});

app.listen(8000);

// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: err
//     });
// });

/**
 * Split ID List
 * @param  {[type]} IDList [description]
 * @return {[type]}        [description]
 */
function splitIdList(IDList) {
  var ssList = IDList.split(/[\n\r,]+/);
  var ssListLen = ssList.length;
  var trimedList = [];
  var cc = 0;
  for (var i = 0; i < ssListLen; i++) {
    if (ssList[i]) {
      var thisOne = ssList[i].toString().replace(/\D/g, '');
      if (thisOne !== '') {
        trimedList[cc] = thisOne;
        cc++;
      }
    }
  }
  return trimedList.join(',\n');
}

/**
 * Form Post
 * @param  {[type]} req        [description]
 * @param  {[type]} res        [description]
 * @param  {[type]} renderPage [description]
 * @return {[type]}            [description]
 */
function ezPost(req, res, sURL, requestData, renderPage) {

  console.log('sURL: ' + sURL);
  console.log('requestData: ' + JSON.stringify(requestData));

  // START: Response after dealing with API
  async.series([
      /*
       * First => deal with API
       */
      function(callback) {
        request({
          uri: sURL,
          method: 'POST',
          json: requestData
        }, function(err, response, body) {
          // console.log('REQUEST RESULTS:', error, response.statusCode, body);
          if (err) {
            console.log(err);
            callback(true);
            return;
          }
          callback(false, new PageData(req.query.fENV, req.query.iCode, requestData, body));
        });
      }
    ],
    /*
     * Collate results
     */
    function(err, results) {
      if (err) {
        console.log(err);
        res.send(500, 'Server Error');
        return;
      }
      res.render(renderPage, results[0]);
    }
  );
  // END: Response after dealing with API
}


function PageData(env, iCode, rdata, jsonString) {
  this.ENV = env;
  this.ICODE = iCode;
  this.RDATA = rdata;
  this.JSONSTRING = JSON.stringify(jsonString);
}
